﻿using System;
using Xamarin.Forms;

namespace TodoREST
{
	public class App : Application
	{
		//public static TodoItemManager TodoManager { get; private set; }
		public static BaseOggetti BO { get; private set; }

		public static ITextToSpeech Speech { get; set; }

		public App ()
		{
			BO = new BaseOggetti (new RestService ());
			MainPage = new NavigationPage (new pgListaOggetti ());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

