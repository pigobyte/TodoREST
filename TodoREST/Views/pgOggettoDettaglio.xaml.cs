﻿using System;
using Xamarin.Forms;
using System.Linq;

namespace TodoREST
{
	public class Categoria
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public System.Collections.Generic.List<CategoraItems> CategoryItem { get; set; }
	}

	public class CategoraItems
	{
		public int Id { get; set; }

		public string ItemName { get; set; }

		public int CatId { get; set; }

		public string ImageUrl { get; set; }
	}

	public partial class pgOggettoDettaglio : ContentPage
	{
		bool isNewItem;

		public System.Collections.Generic.List<Categoria> elencoCategorie = new System.Collections.Generic.List<Categoria>();


		public pgOggettoDettaglio(bool isNew = false)
		{
			InitializeComponent();
			isNewItem = isNew;


			generaElenchi();

		}

		async void OnSaveActivated(object sender, EventArgs e)
		{
			var _oggetto = (unitOggetto)BindingContext;
			await App.BO.SaveTaskAsync(_oggetto, isNewItem);
			await Navigation.PopAsync();
		}

		async void OnDeleteActivated(object sender, EventArgs e)
		{
			var _oggetto = (unitOggetto)BindingContext;
			await App.BO.DeleteTaskAsync(_oggetto);
			await Navigation.PopAsync();
		}

		void OnCancelActivated(object sender, EventArgs e)
		{
			Navigation.PopAsync();
		}

		void OnSpeakActivated(object sender, EventArgs e)
		{
			var _oggetto = (unitOggetto)BindingContext;
			App.Speech.Speak(_oggetto.des_ogg + " " + _oggetto.note_ogg);
		}






		void generaElenchi()
		{
			elencoCategorie.Add(new Categoria()
			{
				Id = 1,
				Name = "Cat1",
				CategoryItem = GetCatItem(1)
			});

			elencoCategorie.Add(new Categoria()
			{
				Id = 2,
				Name = "Cat2",
				CategoryItem = GetCatItem(2)
			});

			elencoCategorie.Add(new Categoria()
			{
				Id = 3,
				Name = "Cat3",
				CategoryItem = GetCatItem(3)
			});

		}

		private System.Collections.Generic.List<CategoraItems> GetCatItem(int catId)
		{
			var item = new System.Collections.Generic.List<CategoraItems>() {
				new CategoraItems () {
					Id = 1,
					ItemName = "Item1",
					CatId = 1,
					ImageUrl = "http://www.adiumxtras.com/images/pictures/stock_person_check_on_x_off_1_18109_6055_image_8917.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.adiumxtras.com/images/pictures/stock_person_check_on_x_off_1_18109_6055_image_8917.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.adiumxtras.com/images/pictures/stock_person_check_on_x_off_1_18109_6055_image_8917.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.adiumxtras.com/images/pictures/stock_person_check_on_x_off_1_18109_6055_image_8917.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.myfirmsapp.co.uk/wp-content/uploads/2014/02/icon-person-circle2-320x320.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.myfirmsapp.co.uk/wp-content/uploads/2014/02/icon-person-circle2-320x320.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.myfirmsapp.co.uk/wp-content/uploads/2014/02/icon-person-circle2-320x320.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1,
					ImageUrl = "http://www.adiumxtras.com/images/pictures/stock_person_check_on_x_off_1_18109_6055_image_8917.png"
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},
				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},

				new CategoraItems () {
					Id = 2,
					ItemName = "Item2",
					CatId = 1
				},

				new CategoraItems () {
					Id = 3,
					ItemName = "Item4",
					CatId = 2,
					ImageUrl = "http://www.adweek.com/fishbowlny/files/original/ariaggna.png"
				},
				new CategoraItems () {
					Id = 4,
					ItemName = "Item5",
					CatId = 2,
					ImageUrl = "http://i.telegraph.co.uk/multimedia/archive/02427/KimJong-un_2427121b.jpg"
				},
				new CategoraItems () {
					Id = 5,
					ItemName = "Item999",
					CatId = 3
				},
			};

			return item.Where(i => i.CatId == catId).ToList();
			;
		}
	}





}
