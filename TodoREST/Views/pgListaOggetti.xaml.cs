﻿using System;
using Xamarin.Forms;
using System.Linq;

namespace TodoREST
{
	public partial class pgListaOggetti : ContentPage
	{
		bool alertShown = false;
		System.Collections.Generic.List<unitOggetto> listaOggetti;

		public pgListaOggetti ()
		{
			InitializeComponent ();
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			if (Constants.RestUrl.Contains ("developer.xamarin.com")) {
				if (!alertShown) {
					await DisplayAlert (
						"Hosted Back-End",
						"This app is running against Xamarin's read-only REST service. To create, edit, and delete data you must update the service endpoint to point to your own hosted REST service.",
						"OK");
					alertShown = true;				
				}
			}

			listaOggetti = await App.BO.CaricaListaOggettiAsync();
			lvOggetti.ItemsSource = listaOggetti;
		}

		void OnAddItemClicked (object sender, EventArgs e)
		{
			var _oggetto = new unitOggetto () {
				ID = Guid.NewGuid ().ToString ()
			};
			var _pgOggettoDettaglio = new pgOggettoDettaglio (true);
			_pgOggettoDettaglio.BindingContext = _oggetto;
			Navigation.PushAsync (_pgOggettoDettaglio);
		}

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var _oggSel = e.SelectedItem as unitOggetto;
			var _pgOggettoDettaglio = new pgOggettoDettaglio (true);
			_pgOggettoDettaglio.BindingContext = _oggSel;

			Navigation.PushAsync (_pgOggettoDettaglio);
		}

		private async void FilterContacts(string filter)
		{
			lvOggetti.BeginRefresh();

			if (string.IsNullOrWhiteSpace(filter))
			{
				lvOggetti.ItemsSource = listaOggetti;
			}
			else
			{
				lvOggetti.ItemsSource = listaOggetti.Where(o => o.des_ogg.ToLower().Contains(filter.ToLower()));

			}

			lvOggetti.EndRefresh();
		}

		void sbSearch_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			sbSearch.TextChanged += (sender2, e2) => FilterContacts(sbSearch.Text);
		}

		void sbSearch_SearchButtonPressed(object sender, System.EventArgs e)
		{
			sbSearch.SearchButtonPressed += (sender2, e2) => FilterContacts(sbSearch.Text);
		}


	}
}
