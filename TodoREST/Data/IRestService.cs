﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoREST
{
	public interface IRestService
	{
		Task<List<unitOggetto>> RefreshDataAsync();

		Task SaveOggettoAsync(unitOggetto item, bool isNewItem);

		Task DeleteOggettoAsync(string id);
	}
}
