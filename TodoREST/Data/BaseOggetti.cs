﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoREST
{
	public class BaseOggetti
	{
		IRestService restService;

		public BaseOggetti (IRestService service)
		{
			restService = service;
		}

		public Task<List<unitOggetto>> CaricaListaOggettiAsync ()
		{
			return restService.RefreshDataAsync ();	
		}

		public Task SaveTaskAsync (unitOggetto item, bool isNewItem = false)
		{
			return restService.SaveOggettoAsync(item, isNewItem);
		}

		public Task DeleteTaskAsync (unitOggetto item)
		{
			return restService.DeleteOggettoAsync (item.ID);
		}
	}
}
