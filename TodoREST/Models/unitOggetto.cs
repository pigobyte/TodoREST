﻿using System;

namespace TodoREST
{
	public class unitOggetto
	{
		public string ID { get; set; }

		public string cod_ogg { get; set; }

		public string des_ogg { get; set; }

		public string note_ogg { get; set; }
	}
}
