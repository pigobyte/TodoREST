﻿using System;

namespace TodoREST
{
	public static class Constants
	{
		// URL of REST service
		//public static string RestUrl = "http://developer.xamarin.com:8081/api/todoitems{0}";
		public static string RestUrl = "http://localhost/slimapi/public/oggetti";
		// Credentials that are hard coded into the REST service
		//public static string Username = "Xamarin";
		//public static string Password = "Pa$$w0rd";
		public static string Username = "root";
		public static string Password = "t00r";
	}
}
